<!doctype html>
<html lang="en">
  <head>
	@vite(['resources/css/app.css', 'resources/js/app.js'])

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Webshop</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.4/font/bootstrap-icons.css%22%3E">
  </head>

  <body style="background-image: linear-gradient(to bottom, #00e9ff, #4ba3d1, #5a6288, #3c2f3d, #000000); min-height: 100vh;">


  <nav class="navbar shadow navbar-expand-lg navbar-light navbar-transparent" >
		<div class="container-fluid">
			<a class="navbar-brand text-white" href="/" > < WebshopName > </a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto">
					<li class="nav-item ">
						<a class="nav-link active mr-5" aria-current="page" href="/login" style="color: #fff;">Login</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/admin">Link</a>
					</li> 
					</li>
					<li class="nav-item">
						<a class="btn btn-primary" href="/cart" style="color: #fff;"> Cart <i class="fa-regular fa-cart-shopping"></i> </a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	
	<div class="table-responsive card container my-4">
    <table class="table table-striped table-hover">
        <thead class="bg-primary text-white">
            <tr>
                <th>Bestellung</th>
                <th>Produkt</th>
                <th>Menge</th>
                <th>Vorname</th>
                <th>Nachname</th>
                <th>Straße</th>
                <th>PLZ</th>
                <th>Land</th>
                <th>E-Mail</th>
                <th>Telefon</th>
                <th>Zahlung</th>
                <th>Gesamtsumme</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $order)
            @foreach ($order as $item)
            <tr>
                <td>{{ $item->order_id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->quantity }}</td>
                <td>{{ $item->firstname }}</td>
                <td>{{ $item->lastname }}</td>
                <td>{{ $item->street }}</td>
                <td>{{ $item->postalCode }}</td>
                <td>{{ $item->country }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->phone }}</td>
                <td>{{ $item->payment }}</td>
                <td>{{ $item->total }}</td>
            </tr>
            @endforeach
            @endforeach
        </tbody>
    </table>
</div>

{{print_r('$data')}}
				



    <!-- Bootstrap JavaScript -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
	<script src="https://kit.fontawesome.com/ed7bab00d1.js" crossorigin="anonymous"></script>
</body>
</html>
