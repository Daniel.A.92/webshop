<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::resource('/', ProductController::class);
Route::resource('/login', UserController::class);
Route::resource('/admin', AdminController::class);
Route::resource('/orders', OrdersController::class);
Route::resource('/cart', CartController::class);
Route::post('/cart/add', [CartController::class, 'addToCart'])->name('cart.add');

Route::get('/delete', function () {
    session()->flush();
    return redirect('/');
});