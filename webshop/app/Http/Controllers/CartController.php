<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $totalPrice = 0;

        $cart = session('cart');

        if (!empty($cart)) {

            foreach (session('cart') as $item) {
                $totalPrice += floatval($item['price']) * floatval($item['quantity']);
            }
        }

        return view('shoppingCart', ['total' => $totalPrice]);
    }

    public function addToCart(Request $request)
    {
        $product = [

            'id' => $request['id'],
            'image' => $request['image'],
            'name' => $request['name'],
            'price' => $request['price'],
            'description' => $request['description'],
        ];

        $cart = session('cart', []);

        $existingProduct = array_search($request['id'], array_column($cart, 'id'));

        if ($existingProduct !== false) {

            $cart[$existingProduct]['quantity']++;

        } else {
            $product['quantity'] = 1;
            $cart[] = $product;
        }

        session()->put('cart', $cart);

        return redirect('/');
    }

    public function showCart()
    {
        $cart = session()->get('cart', []);

        return view('shoppingCart', compact('ShoppingCart'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $orderid = DB::table('orders')->insertGetId([
            'firstname' => $request['firstname'],
            'lastname' => $request['lastname'],
            'street' => $request['street'],
            'postalCode' => $request['postalcode'],
            'country' => $request['country'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'payment' => "Rechnung",
            'total' => $request['total'],
            'created_at' => now()
        ]);

        $userinfo = $request;


        foreach (session('cart', []) as $product) {

            DB::table('order_product')->insert([
                'order_id' => $orderid,
                'product_id' => $product['id'],
                'quantity' => $product['quantity'],
            ]);
        }

        $cart = session('cart', []);

        session()->flush();

        return view('confirmation', ['order' => $userinfo, 'cart' => $cart]);

    }



    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $cart = session('cart', []);

        $existingProduct = array_search($id, array_column($cart, 'id'));

        $cart[$existingProduct]['quantity']++;

        session()->put('cart', $cart);

        return redirect()->route('cart.index')->with('success', 'Cart updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {

        $cart = session('cart', []);

        $existingProduct = array_search($id, array_column($cart, 'id'));

        if ($existingProduct !== false) {
            $cart[$existingProduct]['quantity']--;

            if ($cart[$existingProduct]['quantity'] <= 0) {
                unset($cart[$existingProduct]);
                $cart = array_values($cart);
            }
        }


        session()->put('cart', $cart);

        return redirect('/cart');

    }
}