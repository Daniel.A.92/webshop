<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $product = DB::table('product')->get();
        return view('admin', ['product' => $product]);

    }

    /* public function editview()
    {
    return view('edit');
    }*/

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $product = DB::table('product')->where('id', $request['id'])->first();

        if ($product) {
            return redirect('admin')->with('error', 'Product already exists.');
        }

        if (!$request->hasFile('image')) {
            return redirect('admin')->with('error', 'Please upload an image.');
        }

        $file = $request->file('image');
        $filepath = $file->hashName();
        $file->storeAs('public', $filepath);

        DB::table('product')->insert([
            'id' => $request['id'],
            'name' => $request['name'],
            'price' => $request['price'],
            'description' => $request['description'],
            'image' => $filepath
        ]);

        return redirect('admin');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $databasefile = DB::table('product')->find($id);
        dump($databasefile);
        return view('edit', ['product' => $databasefile]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {

        $product = DB::table('product')->find($id);

        if (!$product) {
            abort(404);
        }

        if ($request->hasFile('image')) {
            $oldpath = $product->image;
            Storage::delete('public/' . $oldpath);

            $updatedfile = $request->file('image');
            $updatedfilepath = $updatedfile->hashName();
            $updatedfile->storeAs('public', $updatedfilepath);

            DB::table('product')->where('id', $id)->update(['image' => $updatedfilepath]);
            $product->image = $updatedfilepath;
        }

        DB::table('product')
            ->where('id', $id)
            ->update([
                'name' => $request['name'],
                'price' => $request['price'],
                'description' => $request['description'],
                'image' => $product->image
            ]);

        return redirect('admin');
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $oldpath = DB::table('product')->find($id)->image;
        Storage::delete('public/' . $oldpath);

        DB::table('product')->where('id', $id)->delete();

        return redirect('admin');
    }
}