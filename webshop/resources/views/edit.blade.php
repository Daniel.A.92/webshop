<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
  </head>
  <body style="background-image: radial-gradient(circle, #00e9ff, #4ba3d1, #5a6288, #3c2f3d, #000000); min-height: 100vh;">

  <nav class="navbar shadow navbar-expand-lg navbar-light navbar-transparent" >
		<div class="container-fluid">
			<a class="navbar-brand" href="/" style="color: #fff;"> < WebshopName > </a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto">
					<li class="nav-item ">
						<a class="nav-link active mr-5" aria-current="page" href="/login" style="color: #fff;">Login</a>
					</li>
				<!--	<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li> -->
					</li>
					<li class="nav-item">
						<a class="btn btn-primary" href="/cart" style="color: #fff;"> Cart <i class="fa-regular fa-cart-shopping"></i> </a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

  <div class="container" style="color: #fff;">
    <h2 class="text-center mt-5"> create/edit </h2>

    <div class="container shadow">
<form action="/admin/{{$product -> id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')
  <div class="mb-3">
    <label for="name" class="form-label">Name:</label>
    <input type="text" value="{{$product -> name}}" class="form-control" id="name" name="name">
  </div>

  <div class="mb-3">
    <label for="price" class="form-label">Price:</label>
    <input type="number" value="{{$product -> price}}"  class="form-control" step="0.01" id="price" name="price">
  </div>

  <div class="mb-3">
    <label for="description" class="form-label">Description:</label>
    <input class="form-control" value="{{$product -> description}}"  id="description" name="description"></input>
  </div>

  <div class="mb-3">
  <label for="image" class="form-label">Image:</label>
  <input type="file" value="{{$product -> image}}" class="form-control" id="image" name="image">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
  </body>
</html>