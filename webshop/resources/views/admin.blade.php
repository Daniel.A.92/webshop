<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AdminView</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
  </head>

  
  <body style="background-image: linear-gradient(to left top, #00e9ff, #4ba3d1, #5a6288, #3c2f3d, #000000); min-height: 100vh;">
  
  <nav class="navbar shadow navbar-expand-lg navbar-light navbar-transparent" >
		<div class="container-fluid">
			<a class="navbar-brand" href="/" style="color: #fff;"> < WebshopName > </a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto">
				<!--	<li class="nav-item ">
						<a class="nav-link active mr-5" aria-current="page" href="/login" style="color: #fff;">Login</a>
					</li> -->
				<li class="nav-item">
					</li> 
        <a class="nav-link active mr-5" aria-current="page" href="/orders" style="color: #fff;">Orders</a>
					</li>
				<!--	<li class="nav-item">
						<a class="btn btn-primary" href="/cart" style="color: #fff;"> Cart <i class="fa-regular fa-cart-shopping"></i> </a>
					</li> -->
				</ul>
			</div>
		</div>
	</nav>




    <div class="d-flex flex-row mt-5 justify-content-center align-items-center">
  <div class="p-5 m-5">

  <h2 class="mb-5" style="color: #fff;">Product overview</h2>

    <table class="table" style="color: #fff;">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">Name</th>
      <th scope="col">Price</th>
      <th scope="col">Description</th>
      <th scope="col">Image</th>
      <th scope="col">Delete/Edit</th>
    </tr>
  </thead>
  @foreach($product as $product)
  <tbody>
   <tr>
      <td>{{$product -> id}}</td> 
      <td>{{$product -> name}}</td> 
      <td>{{$product -> price}}</td> 
      <td>{{$product -> description}}</td> 
      <td> <img src="{{ asset('storage/' .$product->image) }}" alt="Produktbild" style="height: 50px;"> </td>
      <td> <form action="/admin/{{$product -> id}}" method="post">
        @csrf
        @method('delete')
         <button  type="submit" class=" btn btn-danger"> <i class="fa-regular fa-trash"></i> </button>
         <a style="height: 36px; width: 35px;" class="btn btn-warning btn-sm rounded-10" type="button" href="/admin/{{$product->id}}/edit"> <i class="fa-regular fa-pen-to-square"></i> </a>
         </form> </td>
    </tr>
  </tbody> 
  @endforeach
</table>
</div>

<div class="p-5 m-5">

<div class="container" style="color: #fff;">
    <h2 class="text-center mb-2"> Add new product </h2>

<form action="/admin" method="post" enctype="multipart/form-data">
    @csrf
  <div class="mb-3">
    <label for="name" class="form-label">Name:</label>
    <input type="text" class="form-control" id="name" name="name" required>
  </div>

  <div class="mb-3">
    <label for="price" class="form-label">Price:</label>
    <input type="number" class="form-control" step="0.01" id="price" name="price" required>
  </div>

  <div class="mb-3">
    <label for="description" class="form-label">Description:</label>
    <textarea class="form-control" id="description" name="description" required></textarea>
  </div>

  <div class="mb-3">
  <label for="description" class="form-label">Image:</label>
    <input type="file" class="form-control" name="image" required>
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>

</div>
</div>
</div>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
	<script src="https://kit.fontawesome.com/ed7bab00d1.js" crossorigin="anonymous"></script>
  </body>
</html>