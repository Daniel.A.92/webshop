<!doctype html>
<html lang="en">
  <head>
	@vite(['resources/css/app.css', 'resources/js/app.js'])

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Webshop</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.4/font/bootstrap-icons.css%22%3E">
  </head>

  <body style="background-image: linear-gradient(to right bottom, #00e9ff, #4ba3d1, #5a6288, #3c2f3d, #000000); min-height: 100vh;" >

  <style>
  .cardtest{

		transition: 0.3s;
		border-radius:1rem;
		border: solid;
		border-color: white;
		border-width: thin;
	}
    .cardtest:hover{
		scale:1.05;
	}
	.cardtest:active {
		scale:0.905;
}
  </style>
  
	<nav class="navbar shadow navbar-expand-lg navbar-light navbar-transparent" >
		<div class="container-fluid">
			<a class="navbar-brand" href="/" style="color: #fff;"> < WebshopName > </a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto">
					<li class="nav-item ">
						<a class="nav-link active mr-5" aria-current="page" href="/login" style="color: #fff;">Login</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/admin">Link</a>
					</li> 
					</li>
					<li class="nav-item">
						<a class="btn btn-primary" href="/cart" style="color: #fff;"> Cart <i class="fa-regular fa-cart-shopping"></i> </a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	

    <h1 style="font-family: Arial, sans-serif; color: #fff;" class="text-center mt-5"> < WebshopName > </h1>

	

	<div class="container mt-5 mb-5">
		<div class="container">
		<div class="row d-flex justify-content-center">
		@foreach($product as $product)
			<div class="col-md-3 mt-4 p-4 mx-3 shadow cardtest">
				<img src="{{ asset('storage/' .$product->image) }}" class="card-img-top" alt="Produktbild" style="height: 200px;">
					<div class="card-body d-flex flex-column justify-content-between" style="color: #fff;">
						<h5 style="text-decoration: underline;" class="card-title mt-3">{{$product -> name}}</h5>
						<p class="card-text mt-2"> - {{$product -> description}}</p>
						<p class="card-text">Price: {{$product -> price}} € </p>
						<div class="align-items-end">
						<form action="/cart/add" method="post">
							@csrf

						<input type="hidden" name="id" value="{{$product -> id}}">
						<input type="hidden" name="name" value="{{$product -> name}}">
						<input type="hidden" name="description" value="{{$product -> description}}">
						<input type="hidden" name="price" value="{{$product -> price}}">
						<input type="hidden" name="image" value="{{$product -> image}}">

						<button type="submit" class="btn btn-primary float-end"> Add to cart </button>
						</form>
						</div>
					</div>		
			</div>
		@endforeach
		</div>
		</div>
	</div>

	<a href="/delete"> kill session </a>
     
	<!-- Bootstrap JavaScript -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
	<script src="https://kit.fontawesome.com/ed7bab00d1.js" crossorigin="anonymous"></script>
</body>
</html>
