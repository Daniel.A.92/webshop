<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Webshop</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.4/font/bootstrap-icons.css%22%3E">
    <style>
        .white{
            color: white;
        }
        </style>
  </head>

  <body style="background-image: linear-gradient(to bottom, #00e9ff, #4ba3d1, #5a6288, #3c2f3d, #000000); min-height: 100vh;" >



  <nav class="navbar shadow navbar-expand-lg navbar-light navbar-transparent" >
		<div class="container-fluid">
			<a class="navbar-brand" href="/" style="color: #fff;"> < WebshopName > </a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto">
					<li class="nav-item ">
						<a class="nav-link active mr-5" aria-current="page" href="/login" style="color: #fff;">Login</a>
					</li>
				<!--	<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li> -->
					</li>
					<li class="nav-item">
						<a class="btn btn-primary" href="/cart" style="color: #fff;"> Cart <i class="fa-regular fa-cart-shopping"></i> </a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

  <div class="d-flex flex-row mt-5 justify-content-center align-items-center">
  <div class="p-5 m-5">

      <div class="container white">
    <div class="row ">
        <div class="col-lg-12 col-md-12 col-12">
            <h3 class="display-5 mb-2 text-center">Shopping Cart</h3>
            @if(session('cart'))
            <p class="mb-5 text-center">
            <table id="shoppingCart" class="table table-condensed table-responsive white">
                <thead>
                    <tr>
                        <th style="width:60%">Product</th>
                        <th style="width:12%">Price</th>
                        <th style="width:10%">Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(session('cart', []) as $product)
                    <tr>
                        <td data-th="Product">

                            <div class="row">

                                <div class="col-md-3 text-left">
                                    <img src="{{ asset('storage/' .$product['image']) }}" alt="" class="img-fluid d-none d-md-block rounded mb-2 shadow ">
                                </div>

                                <div class="col-md-9 text-left mt-sm-2">
                                    <h4> {{$product['name']}}</h4>
                                    <p class="font-weight-light">{{$product['description']}}</p>
                                </div>

                            </div>
                            
                        </td>

                        <td data-th="Price">{{$product['price']}} € </td>

                        <td data-th="Quantity">
                          <div class="d-flex">

                              <form action="/cart/{{$product['id']}}" method="POST">
                               @csrf
                               @method('patch')
                              <button class="btn btn-success" type="submit">+</button>
                              </form>
                          
                            <button class="btn white">{{$product['quantity']}}</button>

                            <form action="/cart/{{$product['id']}}" method="POST">
                                @csrf
                                @method('delete')
                              <button class="btn btn-danger" type="submit"> - </button>
                              </form>

                          </div>

                        </td>
                    </tr>
                  @endforeach
                    
                </tbody>
            </table>



            <div class="float-end text-right">
                <h4>Subtotal:</h4>
                <h2>{{$total}} €</h2>
            </div>
            @else
                <p>Ihr Warenkorb ist leer.</p>
            @endif
        </div>
    </div>
    <div class="row mt-4 d-flex align-items-center">
        <div class="col-sm-6 order-md-2 text-right">
        </div>
        <div class="col-sm-6 mb-3 mb-m-1 order-md-1 text-md-left">
            <a class="white" href="/">
                <i class="fas fa-arrow-left mr-2 white"></i> Continue Shopping</a>
        </div>
    </div>
</div>
 
</div>



<div class="p-5 m-5 white">
    <h2 class="mb-5"> Shipping and Payment </h2>
      <form action="/cart" method="POST">
        @csrf
  <div class="row ">
    <div class="col">
      <label for="vorname" class="form-label">First name</label>
      <input name="firstname" type="text" class="form-control" id="vorname" required >
    </div>
    <div class="col">
      <label for="nachname" class="form-label">Last name</label>
      <input name="lastname" type="text" class="form-control" id="nachname" required >
    </div>
  </div>
  <div class="mb-3">
    <label for="adresse" class="form-label">Street</label>
    <input name="street" type="text" class="form-control" id="adresse" required>
  </div>
  <div class="row mb-3">
    <div class="col">
    <label for="adresse" class="form-label">PostalCode</label>
      <input name="postalcode" type="text" class="form-control" required >
    </div>
    <div class="col">
    <label for="adresse" class="form-label">Country</label>
      <input name="country" type="text" class="form-control" id="ort" placeholder="" required >
    </div>
  </div>
  <div class="mb-3">
    <label for="adresse" class="form-label">E-Mail</label>
    <input name="email" type="email" class="form-control" id="email" required >
  </div> 
  <div class="mb-3">
    <label for="adresse" class="form-label">Phone</label>
    <input name="phone" type="text" class="form-control" id="phone" required>
  </div>
  <div class="mb-3">
    <label for="bezahloption" class="form-label">Payment</label>
    <select class="form-select" id="bezahloption">
      <option value="">Bitte wählen</option>
      <option value="paypal">PayPal</option>
      <option value="kreditkarte">Kreditkarte</option>
    </select>
    <input type="hidden" name="total" value="{{ $total }}">
  </div>
  <div class="mb-3" id="paypal-inputs" style="display:none;">
    <label for="paypal-email" class="form-label">PayPal E-Mail-Adresse</label>
    <input type="email" class="form-control" id="paypal-email">
  </div>
  <div class="mb-3" id="kreditkarte-inputs" style="display:none;">
    <label for="kreditkarte-nr" class="form-label">Kreditkartennummer</label>
    <input type="text" class="form-control" id="kreditkarte-nr">
    <label for="kreditkarte-ablauf" class="form-label">Ablaufdatum</label>
    <input type="text" class="form-control" id="kreditkarte-ablauf">
    <label for="kreditkarte-cvc" class="form-label">CVC-Code</label>
    <input type="text" class="form-control" id="kreditkarte-cvc">
  </div>
  <button type="submit" href="/confirmation" class="btn btn-primary text-center">Order now</button>
</form>
</div>
</div>
 
  
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
	<script src="https://kit.fontawesome.com/ed7bab00d1.js" crossorigin="anonymous"></script>
</body>
</html>
