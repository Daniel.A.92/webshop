<!doctype html>
<html lang="en">
  <head>
	@vite(['resources/css/app.css', 'resources/js/app.js'])

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Webshop</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.4/font/bootstrap-icons.css%22%3E">
  </head>

  <body style="background-image: radial-gradient(circle, #00e9ff, #4ba3d1, #5a6288, #3c2f3d, #000000); min-height: 100vh;" >



<nav class="navbar shadow navbar-expand-lg navbar-light navbar-transparent" >
		<div class="container-fluid">
			<a class="navbar-brand" href="/" style="color: #fff;"> < WebshopName > </a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ms-auto">
					<li class="nav-item ">
						<a class="nav-link active mr-5" aria-current="page" href="/login" style="color: #fff;">Login</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/admin">Link</a>
					</li> 
					</li>
					<li class="nav-item">
						<a class="btn btn-primary" href="/cart" style="color: #fff;"> Cart <i class="fa-regular fa-cart-shopping"></i> </a>
					</li>
				</ul>
			</div>
		</div>
	</nav>


    <div class="d-flex justify-content-center align-items-center vh-100">
  <div class="card mx-auto d-inline-block mb-4">
    <div class="card-header">
      <h5 class="mb-0">Vielen Dank für deine Bestellung!</h5>
    </div>
    <div class="card-body">
      <p><strong>Name:</strong> {{ $order->firstname }} {{ $order->lastname }}</p>
      <p><strong>Straße:</strong> {{ $order->street }}</p>
      <p><strong>Postleitzahl:</strong> {{ $order->postalcode }}</p>
      <p><strong>Land:</strong> {{ $order->country }}</p>
      <p><strong>E-Mail:</strong> {{ $order->email }}</p>
      <p><strong>Telefon:</strong> {{ $order->phone }}</p>
      <p><strong>Zahlungsmethode:</strong> Rechnung </p>
      <h6><strong>Bestellte Produkte:</strong></h6>
      <ul>
        @foreach ($cart as $product)
        <li>{{ $product['name'] }} ({{ $product['quantity'] }})</li>
        @endforeach
      </ul>
      <p><strong>Gesamtsumme:</strong> {{ $order->total }} EUR</p>
    </div>
  </div>
</div>



    <!-- Bootstrap JavaScript -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
	<script src="https://kit.fontawesome.com/ed7bab00d1.js" crossorigin="anonymous"></script>
</body>
</html>
